function hideEye(){
    let x = document.getElementById("form_pass");
    let y = document.getElementById("openeye");
    let z = document.getElementById("hideeye");

    if(x.type === 'password'){
        x.type = "text";
        y.style.display = "block";
        z.style.display = "none";
    }
    else{
        x.type = "password";
        y.style.display = "none";
        z.style.display = "block";
    }
}


function validator(options){

    function validate(inputElement, rule){   
        var errElement = inputElement.parentElement.querySelector('#messageErr');
        var errMessage = rule.test(inputElement.value);
        
        if(errMessage){
            errElement.innerHTML = errMessage;
            inputElement.parentElement.classList.add('invalid');
        }
        else{
            errElement.innerHTML = '';
            inputElement.parentElement.classList.remove();
        }

    }

    var formElement = document.querySelector(options.form);

    if(formElement){
        options.rules.forEach(function (rule) {
            var inputElement = formElement.querySelector(rule.selector);

            if(inputElement){
                inputElement.onblur = function(){
                    validate(inputElement, rule);
                }
                inputElement.oninput = function(){
                    var errElement = inputElement.parentElement.querySelector('#messageErr');
                    errElement.innerHTML='';
                    inputElement.parentElement.classList.remove();
                }
            }
        });
    }

}

validator.isUsername = function(selector){
    return {
        selector: selector,
        test: function(value){
                return value.trim() ? undefined : 'This account doesnt exist. Please try again!';
        }
    };
}

validator.isPass = function(selector){
    return {
        selector: selector,
        test: function(value){
            return value.trim() ? undefined : 'The password is incorrect. Please try again!';
        }
    };
}

